import os
import subprocess
from datetime import datetime, timezone
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Annotated

import requests
from feedgen.feed import FeedGenerator
from flask import Flask, redirect, render_template, request, url_for
from typing_extensions import TypedDict

app = Flask(__name__)
app.config.from_prefixed_env()

FILE_DIR = Path("static")

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6.1 Safari/605.1.15"
AUTHORIZATION = "Bearer " + app.config["R357_BEARER"]

Millis = Annotated[int, "unix timestamp milliseconds"]


class JsonHost(TypedDict):
    id: int
    photo: str
    background: str
    description: str
    slug: str
    teamMember: bool
    firstname: str
    lastname: str
    podcast_count: None


class JsonProgram(TypedDict):
    id: int
    name: str
    slug: str
    description: str
    advertisement: None
    image_file: str
    authors_visible: bool
    hosts: list[JsonHost]


class JsonTheme(TypedDict):
    id: int
    name: str
    slug: str
    image: str


class JsonEpisode(TypedDict):
    id: int
    title: str
    duration: Annotated[int, "seconds"]
    www: None
    description: str
    recommended: bool
    popular: bool
    listen: bool
    authors: list[JsonHost]
    programs: list[JsonProgram]
    forLoggedIn: bool
    forPatrons: bool
    forPrivileged: bool
    subtitle: str
    audio_file: str
    image_file: str
    created_at: Millis
    published_at: Millis
    themes: list[JsonTheme]


class JsonPodcasts(TypedDict):
    program: JsonProgram
    podcasts: list[JsonEpisode]


class JsonUrl(TypedDict):
    url: str


def get_programs() -> list[JsonProgram]:
    resp = requests.get(
        "https://api.r357.eu/api/podcasts/programs", headers={"User-Agent": USER_AGENT}
    )
    resp.raise_for_status()
    return resp.json()


@app.route("/")
def list_programs():
    programs = get_programs()
    return render_template("programs.html", programs=programs)


def get_program(slug: str) -> JsonPodcasts:
    resp = requests.get(
        f"https://api.r357.eu/api/web/program/slug/{slug}/podcasts",
        headers={"User-Agent": USER_AGENT},
    )
    resp.raise_for_status()
    return resp.json()


@app.route("/program/<slug>")
def rss_program(slug: str):
    program = get_program(slug)
    fg = FeedGenerator()
    fg.load_extension("podcast")
    fg.id(f"https://api.r357.eu/api/web/program/slug/{slug}/rss")
    fg.title(program["program"]["name"])
    for host in program["program"]["hosts"]:
        fg.author(
            name=f"{host['firstname']} {host['lastname']}".strip(),
            uri=f"https://radio357.pl/twoje357/autorzy/{host['slug']}",
        )
    fg.link(href=f"https://radio357.pl/twoje357/audycje/{slug}", rel="alternate")
    fg.logo(program["program"]["image_file"])
    fg.description(program["program"]["description"])
    for podcast in program["podcasts"]:
        fe = fg.add_entry()
        fe.id(f"https://api.r357.eu/api/web/program/slug/{slug}/{podcast['id']}")
        fe.title(podcast["subtitle"])
        fe.description(podcast["description"])
        fe.podcast.itunes_duration(podcast["duration"])
        fe.enclosure(
            "http://" + request.host + url_for("podcast_url", id=podcast["id"]),
            type="audio/mpeg",
        )
        for host in podcast["authors"]:
            fe.author(
                name=f"{host['firstname']} {host['lastname']}".strip(),
                uri=f"https://radio357.pl/twoje357/autorzy/{host['slug']}",
            )
        fe.published(
            datetime.utcfromtimestamp(podcast["published_at"] / 1000).replace(
                tzinfo=timezone.utc
            )
        )
    return fg.rss_str(pretty=True), {"Content-Type": "application/rss+xml"}


@app.route("/url/<int:id>")
def podcast_url(id: int):
    target = FILE_DIR / f"{id}.m4a"
    if not target.exists():
        resp = requests.get(
            f"https://api.r357.eu/api/podcasts/{id}/url",
            headers={"User-Agent": USER_AGENT, "Authorization": AUTHORIZATION},
        )
        resp.raise_for_status()
        j: JsonUrl = resp.json()
        url = j["url"]
        with TemporaryDirectory(dir=FILE_DIR) as td:
            tf = Path(td) / "download.m4a"
            subprocess.run(
                ["ffmpeg", "-i", url, "-c", "copy", "-map", "a", tf],
                check=True,
                universal_newlines=True,
                capture_output=True,
            )
            os.rename(tf, target)
    return redirect(url_for("static", filename=f"{id}.m4a"))


if __name__ == "__main__":
    app.run(debug=True)
